//
//  WaterfallLayout.swift
//  WaterfallPhotos
//
//  Created by Christine on 2020/2/3.
//

import UIKit


enum WaterfallStyle {
    case vertical
    case horizontal
}

class WaterfallLayout: UICollectionViewLayout {
    
    var itemSize: ((IndexPath) -> CGSize) = { _ in
        return .zero
    }
    
    var headerHeight: (_ section: Int) -> CGFloat = { _ in
        return 0
    }
    
    var footerHeight: (_ section: Int) -> CGFloat = { _ in
        return 0
    }
    
    private var flowHeights = [CGFloat]()

    private var flowWidth: CGFloat = 0.0

    private var waterfallWidth: CGFloat = 0.0

    private var attributesArray = [UICollectionViewLayoutAttributes]()
 
    var style: WaterfallStyle = .vertical
    
    var rowMargin: CGFloat = 10
   
    var columnMargin: CGFloat = 10

    var flowCount: Int = 2
    
    var edgeInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    
    override func prepare() {
        super.prepare()
        guard let collectionView = collectionView else {
            return
        }
  
        flowHeights = Array(repeating: edgeInsets.top, count: flowCount)
        attributesArray.removeAll()
     
        prepareValueForCompute()
        
        for section in 0..<collectionView.numberOfSections {
       
            if let attributes = layoutAttributesForSupplementaryView(
                ofKind: UICollectionView.elementKindSectionHeader,
                at: IndexPath(item: 0, section: section)) {
                attributesArray.append(attributes)
            }
            
            for item in 0 ..< collectionView.numberOfItems(inSection: section) {
                let indexPath = IndexPath(item: item, section: section)
                if let attributes = layoutAttributesForItem(at: indexPath) {
                    attributesArray.append(attributes)
                }
            }
            

            if let attributes = layoutAttributesForSupplementaryView(
                ofKind: UICollectionView.elementKindSectionFooter,
                at: IndexPath(item: 0, section: section)) {
                attributesArray.append(attributes)
            }
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return attributesArray
    }
    

    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: elementKind, with: indexPath)
        switch style {
        case .vertical:
            attributes.frame = verticalSupplementaryViewFrame(ofKind: elementKind, at: indexPath)
        case .horizontal:
            attributes.frame = horizontalSupplementaryViewFrame(ofKind: elementKind, at: indexPath)
        }
        return attributes
    }
    
 
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        attributes.frame = itemFrame(with: indexPath)
        return attributes
    }
    

    override var collectionViewContentSize: CGSize {
        switch style {
        case .vertical:
            guard var maxColumnHeight = flowHeights.first else {
                return .zero
            }
            flowHeights.forEach {
                if maxColumnHeight < $0 {
                    maxColumnHeight = $0
                }
            }
            return CGSize(width: 0, height: maxColumnHeight + edgeInsets.bottom)
        case .horizontal:
            guard var maxRowWidth = flowHeights.first else {
                return .zero
            }
            flowHeights.forEach {
                if maxRowWidth < $0 {
                    maxRowWidth = $0
                }
            }
            return CGSize(width: maxRowWidth + edgeInsets.right, height: 0)
        }
    }
}


private extension WaterfallLayout {

    func prepareValueForCompute() {
        guard let collectionView = collectionView else {
            return
        }
        switch style {
        case .vertical:
            waterfallWidth = collectionView.frame.width - edgeInsets.left - edgeInsets.right
            flowWidth = (waterfallWidth - (CGFloat(flowCount) - 1) * columnMargin) / CGFloat(flowCount)
        case .horizontal:
            waterfallWidth = collectionView.frame.height - edgeInsets.top - edgeInsets.bottom
            flowWidth = (waterfallWidth - (CGFloat(flowCount) - 1) * rowMargin) / CGFloat(flowCount)
        }
    }
    
   
    func itemFrame(with indexPath: IndexPath) -> CGRect {
        switch style {
        case .vertical:
            return verticalItemFrame(with: indexPath)
        case .horizontal:
            return horizontalItemFrame(with: indexPath)
        }
    }
    

    func verticalItemFrame(with indexPath: IndexPath) -> CGRect {
    
        let width = flowWidth
        let size = itemSize(indexPath)
        let aspectRatio = size.height / size.width
        let height = width * aspectRatio
        
        var destColumn = 0
        var minColumnHeight = flowHeights[0]
        for (i, v) in flowHeights.enumerated() {
            if v < minColumnHeight {
                minColumnHeight = v
                destColumn = i
            }
        }
        
        let x = edgeInsets.left + CGFloat(destColumn) * (width + columnMargin)
        var y = minColumnHeight
        if y != edgeInsets.top {
            y += rowMargin
        }
        
        let rect = CGRect(x: x, y: y, width: width, height: height)
        
        flowHeights[destColumn] = rect.maxY
        
        return rect
    }
    

    func horizontalItemFrame(with indexPath: IndexPath) -> CGRect {
       
        let height = flowWidth
        let size = itemSize(indexPath)
        let aspectRatio = size.width / size.height
        let width = height * aspectRatio
        
      
        var destRow = 0
        var minRowWidth = flowHeights[0]
        for (i, v) in flowHeights.enumerated() {
            if v < minRowWidth {
                minRowWidth = v
                destRow = i
            }
        }
        

        var x = minRowWidth
        if x != edgeInsets.top {
            x += rowMargin
        }
        let y = edgeInsets.top + CGFloat(destRow) * (height + rowMargin)
        
        let rect = CGRect(x: x, y: y, width: width, height: height)
        
 
        flowHeights[destRow] = rect.maxX
        
        return rect
    }
    

    func verticalSupplementaryViewFrame(ofKind elementKind: String,at indexPath: IndexPath) -> CGRect {
        var y = flowHeights.sorted().last!
        if y == 0 {
            y += edgeInsets.top
        }
        let x = edgeInsets.left
        let w = waterfallWidth
        var h: CGFloat = 0
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            h = headerHeight(indexPath.section)
        default:
            h = footerHeight(indexPath.section)
            y += rowMargin
        }
        let rect = CGRect(x: x, y: y, width: w, height: h)
  
        flowHeights = Array(repeating: rect.maxY, count: flowCount)
        return rect
    }
    
 
    func horizontalSupplementaryViewFrame(ofKind elementKind: String,at indexPath: IndexPath) -> CGRect {
        var x = flowHeights.sorted().last!
        if x == 0 {
            x += edgeInsets.left
        }
        let y = edgeInsets.top
        let h = waterfallWidth
        var w: CGFloat = 0
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            w = headerHeight(indexPath.section)
        default:
            w = footerHeight(indexPath.section)
            x += columnMargin
        }
        let rect = CGRect(x: x, y: y, width: w, height: h)
  
        flowHeights = Array(repeating: rect.maxX, count: flowCount)
        return rect
    }
}

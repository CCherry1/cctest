//
//  ViewController.swift
//  WaterfallPhotos
//
//  Created by Christine on 2020/2/3.
//

import UIKit

class ViewController: UIViewController {

    
    var data: [MyDataModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupData()
        setupUI()
    }

    
    func setupData() {
        let dirName = "\(Bundle.main.bundlePath)/photos.bundle"
        do {
            guard let plistFile = Bundle.main.path(forResource: "photos", ofType: "plist") else {
                return
            }
            let array = NSArray(contentsOf: URL(fileURLWithPath: plistFile)) as? [[String: String]]
            
            let files = try FileManager.default.contentsOfDirectory(atPath: dirName).sorted()
            for file in files {
                print(file)
                let name = String(file.split(separator: ".").first ?? "")
                let img = UIImage(contentsOfFile: "\(dirName)/\(file)")
                let w = img?.size.width ?? 1
                let h = img?.size.height ?? 1
                let screenSize = UIScreen.main.bounds.size
    
                let w1 = (screenSize.width - 30) / 2
                let h1 = w1 / w * h
                let dict = array?.first { $0["Photo"] == name }
                let title = dict?["Caption"]
                let comment = dict?["Comment"]
                let model = MyDataModel(image: img, name: name, height: h1, width: w1, caption: title, comment: comment)
                data.append(model)
            }
            
        } catch {
            assert(false)
        }
    }

    /// UI
    func setupUI() {
        
       let layout = WaterfallLayout()
        layout.itemSize = { [unowned self] indexPath in
            let model = self.data[indexPath.item]
            return CGSize(width: model.width, height: model.height + textHeight)
        }
        layout.headerHeight = { _ in return 20 }
        layout.footerHeight = { _ in return 30 }
        let collectionView = UICollectionView(frame: UIScreen.main.bounds, collectionViewLayout: layout)
        view.backgroundColor = .black
        collectionView.backgroundColor = .black
        collectionView.contentInsetAdjustmentBehavior = .always
        collectionView.dataSource = self
        view.addSubview(collectionView)
        
        
        collectionView.register(PhotoCollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "Header")
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "Footer")
    }
}

extension ViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? PhotoCollectionViewCell
        cell?.backgroundColor = UIColor.random
        let model = data[indexPath.row]
        cell?.bindModel(model: model)
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let hv = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath)
            hv.backgroundColor = .black
            return hv
        case UICollectionView.elementKindSectionFooter:
            let fv = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Footer", for: indexPath)
            fv.backgroundColor = .black
            return fv
        default:
            return UICollectionReusableView(frame: .zero)
        }
    }
}

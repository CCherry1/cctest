//
//  MyDataModel.swift
//  WaterfallPhotos
//
//  Created by Christine on 2020/2/3.
//

import UIKit

struct MyDataModel {

    var image: UIImage?
    var name: String?
    var height: CGFloat = 0
    var width: CGFloat = 0
    var caption: String?
    var comment: String?
}

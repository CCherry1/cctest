//
//  Tookkit.swift
//  WaterfallPhotos
//
//  Created by Christine on 2020/2/3.
//

import UIKit

extension UIColor {
    
    class var random: UIColor {
        let r: CGFloat = CGFloat(arc4random() % 100) / 100
        let g: CGFloat = CGFloat(arc4random() % 100) / 100
        let b: CGFloat = CGFloat(arc4random() % 100) / 100
        return UIColor(red: r, green: g, blue: b, alpha: 1)
    }
    
    class var darkgreen: UIColor {
        let r: CGFloat = 3.0 / 255.0
        let g: CGFloat = 101.0 / 255.0
        let b: CGFloat = 100.0 / 255.0
        return UIColor(red: r, green: g, blue: b, alpha: 1)
    }
}

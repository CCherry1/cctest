//
//  PhotoCollectionViewCell.swift
//  WaterfallPhotos
//
//  Created by Christine on 2020/2/3.
//

import UIKit


let textHeight: CGFloat = 100


class PhotoCollectionViewCell: UICollectionViewCell {
    
    var model: MyDataModel?
    
    lazy var captionLabel: UILabel = {
        let l = UILabel()
        l.textColor = .white
        l.font = UIFont.boldSystemFont(ofSize: 18)
        return l
    }()
    
    lazy var commentLabel: UILabel = {
        let l = UILabel()
        l.textColor = .white
        l.numberOfLines = 3
        l.font = UIFont.systemFont(ofSize: 14)
        return l
    }()
    
    lazy var imageView: UIImageView = {
        let v = UIImageView()
        v.contentMode = UIView.ContentMode.scaleAspectFill
        return v
    }()
    
    lazy var textView: UIView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(imageView)
        textView.backgroundColor = UIColor.darkgreen
        textView.addSubview(captionLabel)
        textView.addSubview(commentLabel)
        addSubview(textView)

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard let model = model else { return }
        imageView.image = model.image
        imageView.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height - textHeight)
        textView.frame = CGRect(x: 0, y: frame.height - textHeight, width: frame.width, height: textHeight)
        captionLabel.frame = CGRect(x: 10, y: 0, width: textView.frame.size.width - 20, height: 30)
        commentLabel.frame = CGRect(x: 10, y: 30, width: textView.frame.size.width - 20, height: textHeight - 30)
    }
    
    
    func bindModel(model: MyDataModel) {
        captionLabel.text = model.caption
        commentLabel.text = model.comment
        self.model = model
    }
}
